import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='spotseeker_restclient',
    version='0.0.2',
    packages=find_packages(),
    include_package_data=True,
    install_requires = [
        'setuptools',
        'Django',
        'urllib3',
        'oauth2',
        'requests-oauthlib',
        'PermissionsLogging',

    ],
    license='Apache License, Version 2.0',  # example license
    description='A Django app for consuming the spotseeker REST API',
    long_description=README,
)
