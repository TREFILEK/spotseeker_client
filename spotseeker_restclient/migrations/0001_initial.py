# Generated by Django 2.1.3 on 2018-11-16 19:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CacheEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service', models.CharField(db_index=True, max_length=50)),
                ('url', models.CharField(db_index=True, max_length=255, unique=True)),
                ('status', models.PositiveIntegerField()),
                ('header_pickle', models.TextField()),
                ('content', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='ItemImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image_id', models.IntegerField()),
                ('description', models.CharField(blank=True, max_length=200)),
                ('display_index', models.PositiveIntegerField(blank=True, null=True)),
                ('width', models.IntegerField()),
                ('height', models.IntegerField()),
                ('content_type', models.CharField(max_length=40)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('upload_user', models.CharField(max_length=40)),
                ('upload_application', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Spot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('spot_id', models.IntegerField()),
                ('name', models.CharField(blank=True, max_length=100)),
                ('uri', models.CharField(max_length=255)),
                ('thumbnail_root', models.CharField(max_length=255)),
                ('latitude', models.DecimalField(decimal_places=8, max_digits=11, null=True)),
                ('longitude', models.DecimalField(decimal_places=8, max_digits=11, null=True)),
                ('height_from_sea_level', models.DecimalField(blank=True, decimal_places=8, max_digits=11, null=True)),
                ('building_name', models.CharField(blank=True, max_length=100)),
                ('floor', models.CharField(blank=True, max_length=50)),
                ('room_number', models.CharField(blank=True, max_length=25)),
                ('building_description', models.CharField(blank=True, max_length=100)),
                ('capacity', models.IntegerField(blank=True, null=True)),
                ('display_access_restrictions', models.CharField(blank=True, max_length=200)),
                ('organization', models.CharField(blank=True, max_length=50)),
                ('manager', models.CharField(blank=True, max_length=50)),
                ('etag', models.CharField(max_length=40)),
                ('last_modified', models.DateTimeField()),
                ('external_id', models.CharField(blank=True, default=None, max_length=100, null=True, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='SpotAvailableHours',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day', models.CharField(max_length=9)),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
            ],
        ),
        migrations.CreateModel(
            name='SpotExtendedInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='SpotImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image_id', models.IntegerField()),
                ('url', models.CharField(max_length=255)),
                ('description', models.CharField(blank=True, max_length=200)),
                ('display_index', models.PositiveIntegerField(blank=True, null=True)),
                ('content_type', models.CharField(max_length=40)),
                ('width', models.IntegerField()),
                ('height', models.IntegerField()),
                ('creation_date', models.DateTimeField()),
                ('modification_date', models.DateTimeField()),
                ('upload_user', models.CharField(max_length=40)),
                ('upload_application', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='SpotItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_id', models.IntegerField()),
                ('name', models.CharField(blank=True, max_length=100)),
                ('category', models.CharField(max_length=255)),
                ('subcategory', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='SpotType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.SlugField()),
            ],
        ),
        migrations.CreateModel(
            name='CacheEntryExpires',
            fields=[
                ('cacheentry_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='spotseeker_restclient.CacheEntry')),
                ('time_expires', models.DateTimeField()),
            ],
            bases=('spotseeker_restclient.cacheentry',),
        ),
        migrations.CreateModel(
            name='CacheEntryTimed',
            fields=[
                ('cacheentry_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='spotseeker_restclient.CacheEntry')),
                ('time_saved', models.DateTimeField()),
            ],
            bases=('spotseeker_restclient.cacheentry',),
        ),
        migrations.AlterUniqueTogether(
            name='cacheentry',
            unique_together={('service', 'url')},
        ),
    ]
